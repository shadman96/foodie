import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Slides } from 'ionic-angular';

/**
 * Generated class for the ProductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {
  @ViewChild(Slides) slides: Slides;

  goToSlide() {
    this.slides.slideTo(1, 500);
  }
  constructor(public navCtrl: NavController, public navParams: NavParams,Slides) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductsPage');
  }
slide = [
    {
      image: "assets/imgs/1.jpg",
    },
    {
       
      image: "assets/imgs/2.jpg",
    },
    {
      
      image: "assets/imgs/4.jpg",
    }
  ];
//   slidesTO(){
//      var i=0;
//      setInterval(function slidechange(carousel){
//          this.image.src=slide[i];
//          if(i<slide.length-1){
//              i++
//          }
// else{
//     i=0;
// }
//      },2000) 
// }
}